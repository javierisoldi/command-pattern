package edu.austral.dissis.patterns.command.case1.app;

import edu.austral.dissis.patterns.command.case1.db.Database;
import edu.austral.dissis.patterns.command.case1.db.Session;

public class DatabaseUtil {
    final Database database;

    public DatabaseUtil(Database database) {
        this.database = database;
    }

    public void createInitialRecords() {
        Session session = null;
        User userObj;
        try {
            session = database.openSession();
            session.beginTransaction();

            for (int j = 101; j <= 105; j++) {
                // Creating User Data & Saving It To The Database
                userObj = new User();
                userObj.setId(j);
                userObj.setName("Editor " + j);
                userObj.setCreatedBy("Administrator");

                session.save(userObj);
            }

            session.getTransaction().commit();
            System.out.println("\n.......Successfully committed.......\n");
        } catch (Exception sqlException) {
            if (null != session.getTransaction()) {
                System.out.println("\n.......Transaction Is Being Rolled Back.......");
                session.getTransaction().rollback();
            }
            sqlException.printStackTrace();
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    public void createNextRecord() {
        Session session = null;
        try {
            session = database.openSession();
            session.beginTransaction();

            final User userObj = new User();
            userObj.setId(106);
            userObj.setName("Editor " + 106);
            userObj.setCreatedBy("Administrator");

            session.save(userObj);

            if (userObj.getId() == 106)
                throw new RuntimeException("Ups... something went wrong!");

            session.getTransaction().commit();
            System.out.println("\n.......Successfully committed.......\n");
        } catch (Exception sqlException) {
            if (null != session.getTransaction()) {
                System.out.println("\n.......Transaction Is Being Rolled Back.......");
                System.out.println("Error: " + sqlException.getMessage());
                session.getTransaction().rollback();
            }
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    public void printAllRecords() {
        Session session = null;
        try {
            session = database.openSession();
            session.beginTransaction();

            session.listAll().forEach(System.out::println);

            session.getTransaction().commit();
           System.out.println("\n.......Successfully committed.......\n");
        } catch (Exception sqlException) {
            if (null != session.getTransaction()) {
                System.out.println("\n.......Transaction Is Being Rolled Back.......");
                System.out.println("Error: " + sqlException.getMessage());
                session.getTransaction().rollback();
            }
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }
}
