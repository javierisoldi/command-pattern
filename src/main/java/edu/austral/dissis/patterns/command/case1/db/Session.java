package edu.austral.dissis.patterns.command.case1.db;

import java.util.List;

public interface Session {
    Transaction beginTransaction();

    Transaction getTransaction();

    void save(Object object);

    List<Object> listAll();

    void close();
}
