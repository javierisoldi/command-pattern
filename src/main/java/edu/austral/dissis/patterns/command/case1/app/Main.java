package edu.austral.dissis.patterns.command.case1.app;

import edu.austral.dissis.patterns.command.case1.db.Database;

public class Main {
    public static void main(String[] args) {
        final Database database = new Database();
        final DatabaseUtil util = new DatabaseUtil(database);

        util.createInitialRecords();
        util.printAllRecords();

        util.createNextRecord();
        util.printAllRecords();
    }
}
