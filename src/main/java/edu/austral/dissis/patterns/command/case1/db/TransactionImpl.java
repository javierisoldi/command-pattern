package edu.austral.dissis.patterns.command.case1.db;

import java.util.function.Function;

public class TransactionImpl implements Transaction {

    private final Runnable onBegan;
    private final Runnable onCommit;
    private final Runnable onRollback;

    private boolean isActive = false;

    public TransactionImpl(Runnable onBegan, Runnable onCommit, Runnable onRollback) {
        this.onBegan = onBegan;
        this.onCommit = onCommit;
        this.onRollback = onRollback;
    }

    @Override
    public void begin() {
        this.onBegan.run();
        isActive = true;
    }

    @Override
    public void commit() {
        this.onCommit.run();
        isActive = false;
    }

    @Override
    public void rollback() {
        this.onRollback.run();
        isActive = false;
    }

    @Override
    public boolean isActive() { return this.isActive; }
}
