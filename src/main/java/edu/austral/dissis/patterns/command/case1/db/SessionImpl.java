package edu.austral.dissis.patterns.command.case1.db;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SessionImpl implements Session {
    private final Database database;

    private Transaction currentTransaction = null;
    private Set<Object> tempSet;

    public SessionImpl(Database database) {
        this.database = database;
    }

    private void handleBegin() {
        tempSet = new HashSet<>(database.getSet());
    }

    private void handleRollback() {
        tempSet = null;
    }

    private void handleCommit() {
        database.setSet(tempSet);
    }

    public Transaction beginTransaction() {
        currentTransaction = new TransactionImpl(this::handleBegin, this::handleCommit, this::handleRollback);
        currentTransaction.begin();

        return currentTransaction;
    }

    @Override
    public Transaction getTransaction() {
        return this.currentTransaction;
    }

    @Override
    public void save(Object object) {
        if (this.currentTransaction == null || !this.currentTransaction.isActive())
            throw new RuntimeException("Transaction has not begin");

        tempSet.add(object);
    }

    @Override
    public List<Object> listAll() {
        final Set<Object> currentSet =
                this.currentTransaction == null || !this.currentTransaction.isActive() ?
                         this.database.getSet() : this.tempSet;

        return new ArrayList<>(currentSet);
    }

    @Override
    public void close() {
    }
}
