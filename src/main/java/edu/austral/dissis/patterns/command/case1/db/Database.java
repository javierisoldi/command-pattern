package edu.austral.dissis.patterns.command.case1.db;

import java.util.HashSet;
import java.util.Set;

public class Database {
    private Set<Object> set = new HashSet<>();

    public Session openSession() { return new SessionImpl(this); }

    public Set<Object> getSet() { return set; }

    public void setSet(Set<Object> set) { this.set = set; }
}
