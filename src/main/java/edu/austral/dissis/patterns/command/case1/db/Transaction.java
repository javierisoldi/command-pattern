package edu.austral.dissis.patterns.command.case1.db;

public interface Transaction {
    void begin();

    void commit();

    void rollback();

    boolean isActive();
}
